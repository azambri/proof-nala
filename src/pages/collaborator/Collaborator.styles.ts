import styled from "styled-components";

export const CollaboratorContainer = styled.div`
  margin: 16px 0;
  padding-bottom: 40px;
`;
export const CardAndButtonContainter = styled.div`
  display: flex;
  .fileButton {
    margin: 80px;
  }
`;
