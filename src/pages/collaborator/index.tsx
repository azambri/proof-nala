import { useEffect, useState } from "react";
import { useSelector } from "react-redux";
import { RootState } from "../../store/store";
import { FormattedMessage } from "react-intl";
import PhotoUploader from "../../components/photoUpload/PhotoUploader";
import Table from "../../components/table/Table";
import Card from "../../components/card/Card";
import user from "../../assets/user.png";
import messages from "../../utils/messages";
import {
  CardAndButtonContainter,
  CollaboratorContainer,
} from "./Collaborator.styles";

const Collaborator = () => {
  const [uniqueEmployeeData, setUniqueEmployeeData] = useState<any>([]);
  const [selectedRow, setSelectedRow] = useState<any>([]);
  const language = "es";
  const employeeData = useSelector(
    (state: RootState) => state.employee.employeeData
  );
  const photos = useSelector((state: RootState) => state.photo.photos);
  useEffect(() => {
    const uniqueEmployeeData = employeeData.filter(
      (obj: { ID: any; Nombre: any }, index: any, self: any[]) =>
        index ===
        self.findIndex((o) => o.ID === obj.ID && o.Nombre === obj.Nombre)
    );

    setUniqueEmployeeData(uniqueEmployeeData);
  }, [employeeData]);

  const handleSearchPhoto = (id: any) => {
    const objeto = photos.find((elemento) => elemento.id === id);
    if (objeto) {
      return objeto.base64Image;
    } else {
      return user;
    }
  };
  return (
    <CollaboratorContainer>
      <FormattedMessage
        id="descriptionThree"
        values={{ greeting: messages[language].descriptionThree }}
      />
      <Table
        data={uniqueEmployeeData}
        special
        selectedRow={selectedRow}
        setSelectedRow={setSelectedRow}
      />
      <CardAndButtonContainter>
        <Card
          title={selectedRow["Nivel Jerárquico"]}
          imageUrl={handleSearchPhoto(selectedRow.ID)}
          name={selectedRow.Nombre}
          area={selectedRow.Area}
          division={selectedRow.División}
          backgroundColor="#9cd2d3"
        />
        <div className="fileButton">
          <FormattedMessage
            id="changeImage"
            values={{ greeting: messages[language].changeImage }}
          />
          <PhotoUploader idPerson={selectedRow.ID} />
        </div>
      </CardAndButtonContainter>
    </CollaboratorContainer>
  );
};

export default Collaborator;
