import styled from "styled-components";

export const StarPageContainer = styled.div`
  display: flex;
  margin: 16px 0;
  padding-bottom: 32px;
`;
