import CSVLoader from "../../components/csvLoader/CsvLoader";
import { StarPageContainer } from "./StartPage.styles";

const StartPage = () => {
  return (
    <StarPageContainer>
      <CSVLoader />
    </StarPageContainer>
  );
};

export default StartPage;
