import styled from "styled-components";
import { makeStyles } from "@material-ui/core/styles";

export const OrganizationChartContainer = styled.div`
  display: flex;
  margin: 16px 0;
  padding-bottom: 32px;
`;
export const FirtsSecondLevel = styled.div`
  display: flex;
`;
export const ContainerSelectorButton = styled.div`
  display: flex;
  justify-content: space-between;
  margin: 16px 0;
`;
export const useStyles = makeStyles((theme) => ({
  formControl: {
    margin: theme.spacing(1),
    minWidth: 120,
  },
  selectEmpty: {
    marginTop: theme.spacing(2),
  },
}));
