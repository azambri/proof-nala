import React, { useEffect, useState } from "react";
import { useSelector } from "react-redux";
import { RootState } from "../../store/store";
import { FormControl, InputLabel, Select, MenuItem } from "@material-ui/core";
import { FormattedMessage } from "react-intl";
import { Button } from "../../components/csvLoader/CsvLoader.styles";
import Table from "../../components/table/Table";
import Card from "../../components/card/Card";
import user from "../../assets/user.png";
import messages from "../../utils/messages";
import {
  OrganizationChartContainer,
  ContainerSelectorButton,
  FirtsSecondLevel,
  useStyles,
} from "./OrganizationChart.styles";

const OrganizationChart = () => {
  const classes = useStyles();
  const [selectedMonth, setSelectedMonth] = useState("");
  const [employeesMonth, setEmployeesMonth] = useState<any>([]);
  const [managerMonth, setManagerMonth] = useState<any>([]);
  const [firstLevel, setFirstLevel] = useState<any>([]);
  const [messArray, setMessArray] = useState<string[] | undefined>(undefined);
  const employeeData = useSelector(
    (state: RootState) => state.employee.employeeData
  );
  const photos = useSelector((state: RootState) => state.photo.photos);
  const language = "es";

  useEffect(() => {
    const meses = employeeData.reduce(
      (acc: { add: (arg0: any) => void }, obj: { Mes: any }) => {
        acc.add(obj.Mes);
        return acc;
      },
      new Set()
    );
    const messArray: string[] = Array.from(meses).map((mes) => String(mes));

    setMessArray(messArray);
  }, [employeeData]);

  useEffect(() => {
    if (selectedMonth) {
      FilteredListMonth();
    }
  }, [selectedMonth]);

  useEffect(() => {
    if (employeesMonth) {
      handleSearchFirtsLevel();
    }
  }, [employeesMonth]);

  const handleChange = (event: React.ChangeEvent<{ value: unknown }>) => {
    const selectedMonth = event.target.value as string;
    setSelectedMonth(selectedMonth);
  };
  const FilteredListMonth: any = () => {
    if (Array.isArray(employeeData)) {
      const filteredItems = employeeData.filter(
        (item) => item.Mes === selectedMonth
      );
      setEmployeesMonth(filteredItems);
      handleSearchManager(filteredItems);
    }
  };

  const handleSearchManager: any = (data: any) => {
    if (Array.isArray(data)) {
      const filteredItems = data.filter(
        (item) => item["Nivel Jerárquico"] === "Manager"
      );
      setManagerMonth(filteredItems);
    }
  };

  const handleSearchFirtsLevel: any = () => {
    if (Array.isArray(employeesMonth)) {
      const filteredItems = employeesMonth.filter(
        (item) => item["ID Lider"] === managerMonth[0].ID
      );
      setFirstLevel(filteredItems);
    }
  };
  const handleSearchSecondLevel: any = (id: string) => {
    if (Array.isArray(employeesMonth)) {
      const filteredItems = employeesMonth.filter(
        (item) => item["ID Lider"] === id
      );
      return filteredItems.map((item: any) => (
        <Card
          title={item["Nivel Jerárquico"]}
          imageUrl={handleSearchPhoto(item.ID)}
          name={item.Nombre}
          area={item.Area}
          division={item.División}
          backgroundColor="#9cd2d3"
        />
      ));
    }
  };
  const handelTotal = employeesMonth.reduce(
    (accumulator: number, empleado: { [x: string]: string }) => {
      const sueldoBruto = parseInt(empleado["Sueldo  bruto"]);
      return accumulator + sueldoBruto;
    },
    0
  );

  const handleSearchPhoto = (id: any) => {
    const objeto = photos.find((elemento) => elemento.id === id);
    if (objeto) {
      return objeto.base64Image;
    } else {
      return user;
    }
  };

  const handlePrint = () => {
    window.print();
  };
  return (
    <div>
      <FormattedMessage
        id="descriptionTwo"
        values={{ greeting: messages[language].descriptionTwo }}
      />
      <div>
        <ContainerSelectorButton>
          <FormControl variant="outlined" className={classes.formControl}>
            <InputLabel id="month-select-label">Month</InputLabel>
            <Select
              labelId="month-select-label"
              id="month-select"
              value={selectedMonth}
              onChange={handleChange}
              label="Month"
            >
              {messArray &&
                messArray.map((month) => (
                  <MenuItem key={month} value={month}>
                    {month}
                  </MenuItem>
                ))}
            </Select>
          </FormControl>
          <Button onClick={handlePrint}>
            <FormattedMessage
              id="buttonPrint"
              values={{ greeting: messages[language].buttonPrint }}
            />
          </Button>
        </ContainerSelectorButton>
        <Table data={employeesMonth} />
      </div>
      <div>
        <FormattedMessage
          id="total"
          values={{ greeting: messages[language].total }}
        />{" "}
        {handelTotal}
      </div>
      <FormattedMessage
        id="organizationchart"
        values={{ greeting: messages[language].organizationchart }}
      />
      <OrganizationChartContainer>
        <div>
          {managerMonth.map((item: any) => (
            <Card
              title={item["Nivel Jerárquico"]}
              imageUrl={handleSearchPhoto(item.ID)}
              name={item.Nombre}
              area={item.Area}
              division={item.División}
            />
          ))}
        </div>
        <div>
          {firstLevel.map((item: any) => (
            <FirtsSecondLevel>
              <Card
                title={item["Nivel Jerárquico"]}
                imageUrl={handleSearchPhoto(item.ID)}
                name={item.Nombre}
                area={item.Area}
                division={item.División}
                backgroundColor="#0799b6"
              />
              {handleSearchSecondLevel(item.ID)}
            </FirtsSecondLevel>
          ))}
        </div>
      </OrganizationChartContainer>
    </div>
  );
};

export default OrganizationChart;
