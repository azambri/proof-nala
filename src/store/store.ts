import { configureStore } from "@reduxjs/toolkit";
import { useDispatch } from "react-redux";
import userReducer from "./slice/userSlice";
import employeeReducer from "./slice/dataSlice";
import photoReducer from "./slice/photoSlice";
import titlesReducer from "./slice/titleSlice";

const store = configureStore({
  reducer: {
    user: userReducer,
    employee: employeeReducer,
    photo: photoReducer,
    titles: titlesReducer,
  },
});

export type AppDispatch = typeof store.dispatch;
export type RootState = ReturnType<typeof store.getState>;
export const useAppDispatch = () => useDispatch<AppDispatch>();
export default store;
