import { createSlice, PayloadAction } from "@reduxjs/toolkit";

const initialState: any = {
  Nombre: "Nombre",
  ID: "ID",
  FechaIn: "Fecha de ingreso",
  Sueldo: "Sueldo bruto",
  División: "División",
  Area: "Área",
  Subarea: "Subárea",
  IDLider: "ID Lider",
  NivelJerarquico: "Nivel Jerárquico",
};

export const titlesSlice = createSlice({
  name: "titles",
  initialState,
  reducers: {
    updateTitle: (state, action: PayloadAction<any>) => {
      const {
        Nombre,
        ID,
        FechaIn,
        Sueldo,
        División,
        Area,
        Subarea,
        IDLider,
        NivelJerarquico,
      } = action.payload;
      state.Nombre = Nombre;
      state.ID = ID;
      state.FechaIn = FechaIn;
      state.Sueldo = Sueldo;
      state.División = División;
      state.Area = Area;
      state.Subarea = Subarea;
      state.IDLider = IDLider;
      state.NivelJerarquico = NivelJerarquico;
    },
  },
});
export const { updateTitle } = titlesSlice.actions;

export default titlesSlice.reducer;
