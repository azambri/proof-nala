import { createSlice, PayloadAction } from "@reduxjs/toolkit";

interface Photo {
  id?: string;
  base64Image: string;
}

interface PhotoState {
  photos: Photo[];
}

const initialState: PhotoState = {
  photos: [],
};

const photoSlice = createSlice({
  name: "photo",
  initialState,
  reducers: {
    addPhoto(state, action: PayloadAction<Photo>) {
      const existingPhotoIndex = state.photos.findIndex(
        (photo) => photo.id === action.payload.id
      );

      if (existingPhotoIndex !== -1) {
        state.photos[existingPhotoIndex] = action.payload;
      } else {
        state.photos.push(action.payload);
      }
    },
  },
});
export const { addPhoto } = photoSlice.actions;
export default photoSlice.reducer;
