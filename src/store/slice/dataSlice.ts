import { createSlice, PayloadAction } from "@reduxjs/toolkit";

const initialState: any = {
  employeeData: [],
};

export const employeeSlice = createSlice({
  name: "employee",
  initialState,
  reducers: {
    setEmployeeData: (state, action: PayloadAction<any[]>) => {
      state.employeeData = action.payload;
    },
    filterEmployeeData: (state, action: PayloadAction<string>) => {
      state.employeeData = state.employeeData.filter(
        (employee: { Mes: string }) => employee.Mes === action.payload
      );
    },
  },
});

export const { setEmployeeData, filterEmployeeData } = employeeSlice.actions;

export default employeeSlice.reducer;
