import styled from "styled-components";

export const ContainerButtons = styled.div`
  display: flex;
  justify-content: space-between;
  margin: 16px 0;
`;

export const DescriptionStart = styled.div`
  font-size: 18px;
  margin: 16px 0;
  width: 768px;
`;
export const Button = styled.button`
  border: none;
  background-color: #5a8dfa;
  color: #ffffff;
  font-size: 18px;
  padding: 5px 20px;
  border-radius: 5px;
  cursor: pointer;
`;
