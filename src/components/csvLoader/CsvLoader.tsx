import { useRef, useState } from "react";
import { useDispatch } from "react-redux";
import { useSelector } from "react-redux";
import { useNavigate } from "react-router-dom";
import Papa from "papaparse";
import Table from "../table/Table";
import { setEmployeeData } from "../../store/slice/dataSlice";
import { RootState } from "../../store/store";
import { FormattedMessage } from "react-intl";
import { ContainerButtons, Button, DescriptionStart } from "./CsvLoader.styles";
import messages from "../../utils/messages";

const CSVLoader = () => {
  const employeeData = useSelector(
    (state: RootState) => state.employee.employeeData
  );
  const navigate = useNavigate();
  const dispatch = useDispatch();
  const [data, setData] = useState<any>(employeeData);
  const [showButton, setShowButton] = useState<boolean>(false);
  const imageRef = useRef<HTMLInputElement>(null);
  const language = "es";

  const handleFileUpload = () => {
    if (imageRef.current && imageRef.current.files) {
      const file = imageRef.current.files[0];
      Papa.parse(file, {
        header: true,
        complete: (result: { data: any }) => {
          setData(result.data);
          setShowButton(true);
        },
      });
    }
  };

  const handleUpdateData = () => {
    dispatch(setEmployeeData(data));
    navigate("/organigrama");
  };

  const handleDownloadCsv = () => {
    const url = process.env.PUBLIC_URL + "/prueba.csv";
    window.open(url);
  };
  return (
    <div>
      <DescriptionStart>
        <FormattedMessage
          id="descriptionStart"
          values={{ greeting: messages[language].descriptionStart }}
        />
      </DescriptionStart>
      <Button onClick={handleDownloadCsv}>
        <FormattedMessage
          id="downloadTemplate"
          values={{ greeting: messages[language].downloadTemplate }}
        />
      </Button>
      <ContainerButtons>
        <input
          type="file"
          accept="text/csv"
          ref={imageRef}
          onChange={handleFileUpload}
        />
        {showButton && (
          <>
            <div>
              <FormattedMessage
                id="confirm"
                values={{ greeting: messages[language].confirm }}
              />
            </div>
            <Button onClick={handleUpdateData}>
              <FormattedMessage
                id="uploaddata"
                values={{ greeting: messages[language].uploaddata }}
              />
            </Button>
          </>
        )}
      </ContainerButtons>
      <Table data={data} />
    </div>
  );
};

export default CSVLoader;
