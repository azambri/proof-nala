import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableContainer from "@material-ui/core/TableContainer";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import Paper from "@material-ui/core/Paper";
import { TextField } from "@material-ui/core";
import { useSelector } from "react-redux";
import { RootState } from "../../store/store";
import { useState } from "react";
import { Button } from "../csvLoader/CsvLoader.styles";
import { useDispatch } from "react-redux";
import { updateTitle } from "../../store/slice/titleSlice";
import { useStyles } from "./Table.styles";

const DataTable = ({
  data,
  special = false,
  selectedRow,
  setSelectedRow,
}: any) => {
  const classes = useStyles();
  const handleRowClick = (rowData: any) => {
    setSelectedRow(rowData);
  };
  const titles = useSelector((state: RootState) => state.titles);
  const [formValues, setFormValues] = useState<any>(titles);
  const dispatch = useDispatch();
  const handleInputChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    setFormValues({
      ...formValues,
      [event.target.name]: event.target.value,
    });
  };
  const handleUpdateTitle = () => {
    dispatch(updateTitle(formValues));
  };

  return (
    <TableContainer component={Paper}>
      <Table className={classes.table} aria-label="simple table">
        <TableHead>
          <TableRow>
            {!special && <TableCell>Mes</TableCell>}

            {special ? (
              <>
                <TableCell>
                  <TextField
                    label="Nombre"
                    name="Nombre"
                    value={formValues.Nombre}
                    onChange={handleInputChange}
                  />
                </TableCell>
                <TableCell>
                  <TextField
                    label="ID"
                    name="ID"
                    value={formValues.ID}
                    onChange={handleInputChange}
                  />
                </TableCell>
                <TableCell>
                  <TextField
                    label="FechaIn"
                    name="FechaIn"
                    value={formValues.FechaIn}
                    onChange={handleInputChange}
                  />
                </TableCell>
                <TableCell>
                  <TextField
                    label="Sueldo"
                    name="Sueldo"
                    value={formValues.Sueldo}
                    onChange={handleInputChange}
                  />
                </TableCell>
                <TableCell>
                  <TextField
                    label="División"
                    name="División"
                    value={formValues.División}
                    onChange={handleInputChange}
                  />
                </TableCell>
                <TableCell>
                  <TextField
                    label="Area"
                    name="Area"
                    value={formValues.Area}
                    onChange={handleInputChange}
                  />
                </TableCell>
                <TableCell>
                  <TextField
                    label="Subarea"
                    name="Subarea"
                    value={formValues.Subarea}
                    onChange={handleInputChange}
                  />
                </TableCell>
                <TableCell>
                  <TextField
                    label="IDLider"
                    name="IDLider"
                    value={formValues.IDLider}
                    onChange={handleInputChange}
                  />
                </TableCell>
                <TableCell>
                  <TextField
                    label="NivelJerarquico"
                    name="NivelJerarquico"
                    value={formValues.NivelJerarquico}
                    onChange={handleInputChange}
                  />
                </TableCell>
                <TableCell>
                  <Button onClick={handleUpdateTitle}>update</Button>
                </TableCell>
              </>
            ) : (
              <>
                <TableCell>{titles.Nombre}</TableCell>
                <TableCell>{titles.ID}</TableCell>
                <TableCell>{titles.FechaIn}</TableCell>
                <TableCell>{titles.Sueldo}</TableCell>
                <TableCell>{titles.División}</TableCell>
                <TableCell>{titles.Area}</TableCell>
                <TableCell>{titles.Subarea}</TableCell>
                <TableCell>{titles.IDLider}</TableCell>
                <TableCell>{titles.NivelJerarquico}</TableCell>
              </>
            )}
          </TableRow>
        </TableHead>
        <TableBody>
          {data.map((row: any) => (
            <TableRow
              key={row.ID}
              style={{
                backgroundColor: selectedRow?.ID === row.ID ? "lightblue" : "",
              }}
              onClick={special ? () => handleRowClick(row) : () => {}}
            >
              {!special && (
                <TableCell component="th" scope="row">
                  {row.Mes}
                </TableCell>
              )}
              <TableCell>{row.Nombre}</TableCell>
              <TableCell>{row.ID}</TableCell>
              <TableCell>{row["Fecha de ingreso"]}</TableCell>
              <TableCell>{row["Sueldo  bruto"]}</TableCell>
              <TableCell>{row.División}</TableCell>
              <TableCell>{row.Area}</TableCell>
              <TableCell>{row.Subarea}</TableCell>
              <TableCell>{row["ID Lider"]}</TableCell>
              <TableCell>{row["Nivel Jerárquico"]}</TableCell>
            </TableRow>
          ))}
        </TableBody>
      </Table>
    </TableContainer>
  );
};

export default DataTable;
