import { CardContainer, Title, Image, Subtitle } from "./Card.styles";
import { CardProps } from "./model";
const Card = ({
  title,
  imageUrl,
  area,
  backgroundColor,
  division,
  name,
}: CardProps) => {
  return (
    <CardContainer color={backgroundColor}>
      <Title>{title}</Title>
      <Subtitle>Nombre: {name}</Subtitle>
      <Image src={imageUrl} alt={title} />
      <Subtitle>Area: {area}</Subtitle>
      <Subtitle>Division: {division}</Subtitle>
    </CardContainer>
  );
};

export default Card;
