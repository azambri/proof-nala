export interface CardProps {
  title: string;
  name: string;
  imageUrl: string;
  area: string;
  division: string;
  backgroundColor?: string;
}
