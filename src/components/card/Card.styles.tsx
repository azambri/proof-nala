import styled from "styled-components";

interface ICardContainer {
  color?: string;
}
export const CardContainer = styled.div<ICardContainer>`
  display: flex;
  flex-direction: column;
  justify-content: flex-start;
  align-items: center;
  padding: 16px;
  width: 200px;
  background-color: ${({ color }) => color || "#1a237e"};
  color: #fff;
  border-radius: 8px;
  margin: 16px;
`;

export const Title = styled.h2`
  font-size: 24px;
  font-weight: bold;
  margin-bottom: 8px;
`;

export const Image = styled.img`
  width: 80px;
  height: 80px;
  margin-bottom: 16px;
  border-radius: 50%;
`;

export const Subtitle = styled.span`
  font-size: 18px;
  margin-bottom: 8px;
`;
