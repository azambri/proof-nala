import styled from "styled-components";

export const FooterContainer = styled.div`
  position: fixed;
  bottom: 0;
  left: 0;
  right: 0;
  height: 20px;
  background-color: #ffffff;
  box-shadow: 0px -2px 2px 0px rgba(0, 0, 0, 0.2);
  display: flex;
  justify-content: center;
  align-items: center;
  padding: 0 20px;
  z-index: 2;
  flex-direction: column;
  padding: 16px 0;
`;
