import { FooterContainer } from "./Footer.styles";

const Footer = () => {
  return (
    <FooterContainer>
      <a href="https://www.linkedin.com/in/alex-eduardo-zambrano-perez-aa445915b/">
        <h6>by Alex Eduardo Zambrano Perez</h6>
      </a>
    </FooterContainer>
  );
};

export default Footer;
