import React, { useState } from "react";
import { useDispatch } from "react-redux";
import { addPhoto } from "../../store/slice/photoSlice";
import { IPhotoUploader } from "./model";

const PhotoUploader = ({ idPerson }: IPhotoUploader) => {
  const dispatch = useDispatch();
  const [base64Image, setBase64Image] = useState("");

  const handleFileChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    const files = event.target.files;
    if (files) {
      const reader = new FileReader();
      reader.readAsDataURL(files[0]);
      reader.onload = () => {
        const base64Image = reader.result?.toString();
        if (base64Image) {
          setBase64Image(base64Image);
        }
      };
    }
  };

  const handleAddPhoto = () => {
    dispatch(addPhoto({ id: idPerson, base64Image }));
    setBase64Image("");
  };

  return (
    <div>
      <input type="file" onChange={handleFileChange} accept="image/*" />
      <button disabled={!idPerson || !base64Image} onClick={handleAddPhoto}>
        Add Photo
      </button>
    </div>
  );
};

export default PhotoUploader;
