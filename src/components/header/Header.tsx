import React from "react";
import { HeaderProps } from "./model";
import { HeaderContainer, Logo, Button } from "./Header.styles";

const Header: React.FC<HeaderProps> = ({ logoSrc, buttonText, onClick }) => {
  return (
    <>
      <HeaderContainer>
        <Logo src={logoSrc} />
        <Button onClick={onClick}>{buttonText}</Button>
      </HeaderContainer>
    </>
  );
};

export default Header;
