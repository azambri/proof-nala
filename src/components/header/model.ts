export interface HeaderProps {
  logoSrc: string;
  buttonText: string;
  onClick: () => void;
}
