import React, { useState } from "react";

import { Link } from "react-router-dom";
import { FormattedMessage } from "react-intl";
import messages from "../../utils/messages";
import { Tabs, TabButton } from "./Tabmenu.styles";

const TabMenu = () => {
  const [activeTab, setActiveTab] = useState("tab1");
  const language = "es";
  const handleTabClick = (tab: React.SetStateAction<string>) => {
    setActiveTab(tab);
  };

  return (
    <Tabs>
      <Link to="/">
        <TabButton
          active={activeTab === "tab1"}
          onClick={() => handleTabClick("tab1")}
        >
          <FormattedMessage
            id="start"
            defaultMessage="Inicio"
            description="A greeting"
            tagName="p"
            values={{ greeting: messages[language].start }}
          />
        </TabButton>
      </Link>
      <Link to="/organigrama">
        <TabButton
          active={activeTab === "tab2"}
          onClick={() => handleTabClick("tab2")}
        >
          <FormattedMessage
            id="organizationchart"
            defaultMessage="Organigrama"
            description="A organizationchart"
            tagName="p"
            values={{ greeting: messages[language].organizationchart }}
          />
        </TabButton>
      </Link>
      <Link to="/colaborador">
        <TabButton
          active={activeTab === "tab3"}
          onClick={() => handleTabClick("tab3")}
        >
          <FormattedMessage
            id="collaborator"
            defaultMessage="Colaborador"
            description="A collaborator"
            tagName="p"
            values={{ greeting: messages[language].collaborator }}
          />
        </TabButton>
      </Link>
    </Tabs>
  );
};

export default TabMenu;
