import styled from "styled-components";
interface ITabButton {
  active: boolean;
}
export const Tabs = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;
  border-bottom: 2px solid #f1f5f9;
  border-radius: 8px;
`;

export const TabButton = styled.button<ITabButton>`
  background-color: ${({ active }) => (active ? "#7694fa" : "#f1f5f9")};
  border: none;
  cursor: pointer;
  font-size: 16px;
  padding: 1px 25px;
  outline: none;
  color: ${({ active }) => (active ? "#ffffff" : "black")};
  border-radius: ${({ active }) => (active ? "8px" : "0")};
  text-transform: uppercase;
`;
