import { IMessages } from "./model";

const messages: IMessages = {
  en: {
    greeting: "Hello",
    buttonText: "Click me",
    start: "start",
    organizationchart: "organization chart",
    collaborator: "collaborator",
    descriptionStart:
      "To start, upload a CSV file. In the following link, you will find a template that you can download as a model. Remember not to include fields with spaces for the proper functioning of the app",
    downloadTemplate: "Download template",
    confirm:
      "If your data is correct, you can click on the 'upload data' button",
    uploaddata: "upload data",
    descriptionTwo: "To begin, select a month.",
    buttonPrint: "Print",
    total: "Total payroll for the month",
    descriptionThree: "To begin, select a employee",
    changeImage: "To change the employee's image, select an image file",
  },
  es: {
    greeting: "Hola",
    buttonText: "Haz clic",
    start: "Inicio",
    organizationchart: "Organigrama",
    collaborator: "colaborador",
    descriptionStart:
      "Para iniciar, suba un archivo csv. En el siguiente enlace encontrará una plantilla que puede descargar como modelo. Recuerde no incluir campos con espacios para que la aplicación funcione correctamente.",
    downloadTemplate: "Descargar plantilla",
    confirm: "si su data es correcta puede dar click en 'subir datos'",
    uploaddata: "subir datos",
    descriptionTwo: "Para iniciar seleccione un mes",
    buttonPrint: "Imprimir",
    total: "Total Nomina mes",
    descriptionThree: "Para iniciar seleccione un empleado",
    changeImage:
      "Para cambiar la imagen del empleado seleccione un archivo de imagen",
  },
};

export default messages;
