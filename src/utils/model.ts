export interface IMessages {
  [key: string]: {
    greeting: string;
    buttonText: string;
    start: string;
    organizationchart: string;
    collaborator: string;
    descriptionStart: string;
    downloadTemplate: string;
    confirm: string;
    uploaddata: string;
    descriptionTwo: string;
    buttonPrint: string;
    total: string;
    descriptionThree: string;
    changeImage: string;
  };
}
